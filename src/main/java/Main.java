import Enums.DayOfWeek;
import Enums.Species;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


    public class Main {
        public static void main(String[] args) throws ParseException {
            List<Family> families = new ArrayList<>();
            FamilyController familyController = new FamilyController(new FamilyService(new CollectionFamilyDao(families)));
            familyController.start();

        }
    }


