import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;


public class FamilyController {
        private FamilyService familyService;

        public FamilyController(FamilyService familyService) {
            this.familyService = familyService;
        }

        public FamilyController() {
        }

        public void start() throws ParseException {
         while(true){
             System.out.println("       ******MENU******");
             System.out.println("1. Заповнити тестовими даними ");
             System.out.println("2. Відобразити весь список сімей ");
             System.out.println("3. Відобразити список сімей, де кількість людей більша за задану");
             System.out.println("4. Відобразити список сімей, де кількість людей менша за задану");
             System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює");
             System.out.println("6. Створити нову родину");
             System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку");
             System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку ");
             System.out.println("9. УВидалити всіх дітей старше віку ");
             System.out.println("10 Зберегти дані");
             System.out.println("10. Exit");


             System.out.println("Введіть число");
             Scanner in = new Scanner(System.in);
             if (!in.hasNextInt()) {
                 System.out.println("Введіть число від 1 до 10");
                 continue;
             }
             int menu = in.nextInt();
             while (menu < 1 || menu > 10) {
                 System.out.println("Ви ввели неіснуючий пункт, введіть число від 1 до 10");
                 menu = in.nextInt();
             }
             switch (menu){
                 case 1:
                     familyService.getCollectionFamilyDao().setFamilies(AutoFamily.AutoFamilyCreator());
                     System.out.println("База заповнена автоматично");

                     break;
                 case 2:
                     displayAllFamilies();
                     break;
                 case 3:
                     System.out.println("Введіть кількість людей:");
                     int number = in.nextInt();

                 case 4:
                     System.out.println("Введіть кількість людей:");
                     int number2 = in.nextInt();

                     break;


                 case 5:
                     break;

                 case 6:
                     System.out.println("Введіть імя матері: ");
                     String mothersName = in.nextLine();
                     System.out.println("Введіть фамілію матері ");
                     String mothersSurname = in.nextLine();
                     System.out.println("Введіть дату народження матері ");
                     String date = in.nextLine();
                     System.out.println("Введіть IQ матері: ");
                     int mothersIq = in.nextInt();
                     System.out.println("Введіть имя батька: ");
                     String fathersName = in.nextLine();
                     System.out.println("Введіть фамилию батька: ");
                     String fathersSurname = in.nextLine();
                     System.out.println("Введіть дату народження батька: ");
                     String dateFather = in.nextLine();
                     System.out.println("Введіть IQ батька: ");
                     int fathersIq = in.nextInt();
                     createNewFamily(new Woman(mothersName, mothersSurname, date, mothersIq), new Man(fathersName,fathersSurname,dateFather,fathersIq));
                     break;

                 case 7:
                     break;

                 case 8:
                     break;

                 case 9:
                     break;
                 case 10:
                     try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("family"))) {
                         objectOutputStream.writeObject(familyService.getAllFamilies());
                     } catch (IOException e) {
                         throw new RuntimeException(e);
                     }
                     break;

             }



         }



        }

        public void setFamilyService(FamilyService familyService) {
            this.familyService = familyService;
        }

        public List getAllFamilies() {
            return familyService.getAllFamilies();
        }

        public void displayAllFamilies() {
            System.out.println(familyService.getAllFamilies());;
        }

        public List getFamiliesBiggerThan(int quantity) {
            return familyService.getFamiliesBiggerThan(quantity);
        }

        public List getFamiliesLessThan(int quantity) {
            return familyService.getFamiliesLessThan(quantity);
        }

        public int countFamiliesWithMemberNumber(int quantity) {
            return familyService.countFamiliesWithMemberNumber(quantity);
        }

        public void createNewFamily(Human mother, Human father) {
            familyService.createNewFamily(mother,father);
        }

       public Family bornChild(Family family, String manName, String womanName) {
            return familyService.bornChild(family,manName,womanName);
       }

        public Family adoptChild(Family family, Human child) {
            return familyService.adoptChild(family, child);
        }

        public void deleteAllChildrenOlderThen(int year) {
            familyService.deleteAllChildrenOlderThen(year);
        }

        public int count() {
            return familyService.count();
        }

        public HashSet<Pet> getPets (int index) {
            return familyService.getCollectionFamilyDao().getFamilyByIndex(index).getPetsSet();
        }

        public void addPet(int indexOfFamily, Pet pet) {
            familyService.getCollectionFamilyDao().addPet(indexOfFamily,pet);
        }
    }



