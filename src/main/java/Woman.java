import Enums.DayOfWeek;

import java.util.HashMap;

public final class Woman extends Human{

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman(String name, String surname, int year, int iq,  HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman() {

    }

    public Woman(String mothersName, String mothersSurname, String date, int mothersIq) {
        super(mothersName, mothersSurname, Integer.parseInt(date), mothersIq);
    }

    @Override
    public void greetPet() {
        super.greetPet();
    }
    public void makeup(){
        System.out.println("подкрасится");
    }
}
