import Enums.DayOfWeek;
import Enums.Species;

import java.text.ParseException;
import java.util.*;

public class AutoFamily {
    public static List<Family> AutoFamilyCreator() throws ParseException {
        List<Family> families = new ArrayList<>();
        Man viktor = new Man("Viktor","Munko", 40);
        Woman halyna = new Woman("Halyna","Munko",89);
        Dog dog = new Dog("Dog", 2, 12,  new HashSet<String>(Set.of("rrr")));
        Man tymur = new Man("Tymur", "Munko", 12);
        Man bohdan = new Man("Bohdan", "Munko", 7);
        Family familyWick = new Family(viktor, halyna);
        familyWick.addChild(tymur);
        familyWick.addChild(bohdan);
        HashSet<Pet> petsWick = new HashSet<>();
        petsWick.add(dog);
        familyWick.setPetsSet(petsWick);




        families.add(familyWick);
        return families;

    }

}
