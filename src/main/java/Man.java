import Enums.DayOfWeek;

import java.util.HashMap;

public final class Man extends Human {

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Man(String name, String surname, int year, int iq, HashMap<DayOfWeek, String> schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Man() {

    }

    public Man(String fathersName, String fathersSurname, String dateFather, int fathersIq) {
        super(fathersName,fathersSurname, Integer.parseInt(dateFather),fathersIq);
    }


    @Override
    public void greetPet() {
        super.greetPet();
    }
    public void repairCar(){
        System.out.println("чинить авто");
    }
}
